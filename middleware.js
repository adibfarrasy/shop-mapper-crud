const { shopSchema, reviewSchema } = require("./joiSchemas");
const ExpressError = require("./utils/ExpressError");
const Shop = require("./models/shop");
const Review = require("./models/review");

module.exports.isLoggedIn = (req, res, next) => {
  if (!req.isAuthenticated()) {
    req.session.returnTo = req.originalUrl;
    req.flash("error", "You must be signed in.");
    return res.redirect("/login");
  }
  next();
};

module.exports.validateShop = (req, res, next) => {
  // Server-side val
  const { error } = shopSchema.validate(req.body);
  if (error) {
    const msg = error.details.map((el) => el.message).join(",");
    throw new ExpressError(msg, 400);
  } else {
    next();
  }
};

module.exports.isAuthor = async (req, res, next) => {
  const { id } = req.params;
  const shop = await Shop.findById(id);

  if (!shop.author.equals(req.user._id)) {
    req.flash("error", "You do not have the permission to do that.");
    return res.redirect(`/shops/${id}`);
  }
  next();
};

module.exports.validateReview = (req, res, next) => {
  const { error } = reviewSchema.validate(req.body);
  if (error) {
    const msg = error.details.map((el) => el.message).join(",");
    throw new ExpressError(msg, 400);
  } else {
    next();
  }
};

module.exports.isReviewAuthor = async (req, res, next) => {
  const { id, reviewId } = req.params;
  const review = await Review.findById(reviewId);

  if (!review.author.equals(req.user._id)) {
    req.flash("error", "You do not have the permission to do that.");
    return res.redirect(`/shops/${id}`);
  }
  next();
};
