if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const mongoose = require("mongoose");
const cities = require("./cities");
const Shop = require("../models/shop");
const Review = require("../models/review");
const { places, descriptors } = require("./seedHelpers");
const dbUrl = process.env.DB_URL;

mongoose.connect(dbUrl, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => {
  console.log("Database connected");
});

const sample = (arr_arg) => arr_arg[Math.floor(Math.random() * arr_arg.length)];

const seedDB = async () => {
  await Shop.deleteMany({}); // clear all entries
  await Review.deleteMany({});
  for (let i = 0; i < 200; i++) {
    const random516 = Math.floor(Math.random() * 516);
    const price = Math.floor(Math.random() * 10000) + 10000;
    const shop_obj = new Shop({
      author: `610ce5b2453e68001543d933`, // My admin's ObjectId
      location: `${cities[random516].city}`,
      title: `${sample(descriptors)} ${sample(places)}`,
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis temporibus impedit necessitatibus nostrum earum similique tempore dolor voluptates. Assumenda saepe delectus velit impedit hic? Necessitatibus tenetur nemo porro velit ab. Vel, incidunt consequatur deleniti quo facilis natus accusantium similique ipsam doloribus suscipit commodi ullam sapiente! Eligendi, molestiae eveniet mollitia iusto nam neque alias qui quod eum quia, possimus, ullam earum.",
      price,
      geometry: {
        type: "Point",
        coordinates: [cities[random516].longitude, cities[random516].latitude],
      },
      images: [
        {
          url: "https://images.unsplash.com/photo-1601647998485-76bd9e0596c8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fHNob3BzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&h=300&q=60",
          filename: "Intereshop/studioshop_xruhsy.jpg",
        },
        {
          url: "https://images.unsplash.com/photo-1598627346159-2e4b4da2cbb5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTl8fHNob3BzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&h=300&q=60",
          filename: "Intereshop/coffeeshop_slebrx.jpg",
        },
      ],
    });

    await shop_obj.save();
  }
};

seedDB().then(() => {
  mongoose.connection.close();
});
