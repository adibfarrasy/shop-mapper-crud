module.exports = [
    {
      city: "Kab. Aceh barat",
      latitude: 4.4543,
      longitude: 96.1527
    },
    {
      city: "Kab. Aceh barat daya",
      latitude: 3.7963,
      longitude: 97.0068
    },
    {
      city: "Kab. Aceh besar",
      latitude: 5.4529,
      longitude: 95.4778
    },
    {
      city: "Kab. Aceh jaya",
      latitude: 4.7874,
      longitude: 95.6458
    },
    {
      city: "Kab. Aceh selatan",
      latitude: 3.3115,
      longitude: 97.3517
    },
    {
      city: "Kab. Aceh singkil",
      latitude: 2.3589,
      longitude: 97.8722
    },
    {
      city: "Kab. Aceh tamiang",
      latitude: 4.2329,
      longitude: 98.0029
    },
    {
      city: "Kab. Aceh tengah",
      latitude: 4.4483,
      longitude: 96.8351
    },
    {
      city: "Kab. Aceh tenggara",
      latitude: 3.3089,
      longitude: 97.6982
    },
    {
      city: "Kab. Aceh timur",
      latitude: 4.5224,
      longitude: 97.6114
    },
    {
      city: "Kab. Aceh utara",
      latitude: 4.9786,
      longitude: 97.2221
    },
    {
      city: "Kab. Bener meriah",
      latitude: 4.7514,
      longitude: 96.9525
    },
    {
      city: "Kab. Bireuen",
      latitude: 5.1086,
      longitude: 96.6638
    },
    {
      city: "Kab. Gayo lues",
      latitude: 3.9552,
      longitude: 97.3517
    },
    {
      city: "Kab. Nagan raya",
      latitude: 4.1248,
      longitude: 96.493
    },
    {
      city: "Kab. Pidie",
      latitude: 5.0743,
      longitude: 95.941
    },
    {
      city: "Kab. Pidie jaya",
      latitude: 5.1548,
      longitude: 96.1951
    },
    {
      city: "Kab. Simeulue",
      latitude: 2.644,
      longitude: 96.0256
    },
    {
      city: "Kota banda aceh",
      latitude: 5.5483,
      longitude: 95.3238
    },
    {
      city: "Kota langsa",
      latitude: 4.4725,
      longitude: 97.9756
    },
    {
      city: "Kota lhokseumawe",
      latitude: 5.1812,
      longitude: 97.1413
    },
    {
      city: "Kota sabang",
      latitude: 5.8926,
      longitude: 95.3238
    },
    {
      city: "Kota subulussalam",
      latitude: 2.7121,
      longitude: 97.9157
    },
    {
      city: "Kab. Asahan",
      latitude: 2.8175,
      longitude: 99.6341
    },
    {
      city: "Kab. Batubara",
      latitude: 3.1741,
      longitude: 99.5006
    },
    {
      city: "Kab. Dairi",
      latitude: 2.8676,
      longitude: 98.2651
    },
    {
      city: "Kab. Deli serdang",
      latitude: 3.4202,
      longitude: 98.7041
    },
    {
      city: "Kab. Humbang hasundutan",
      latitude: 2.1989,
      longitude: 98.5721
    },
    {
      city: "Kab. Karo",
      latitude: 3.1053,
      longitude: 98.2651
    },
    {
      city: "Kab. Labuhanbatu",
      latitude: 2.344,
      longitude: 100.1703
    },
    {
      city: "Kab. Labuhanbatu selatan",
      latitude: 1.8799,
      longitude: 100.1703
    },
    {
      city: "Kab. Labuhanbatu utara",
      latitude: 2.3466,
      longitude: 99.8125
    },
    {
      city: "Kab. Langkat",
      latitude: 3.8654,
      longitude: 98.3088
    },
    {
      city: "Kab. Mandailing natal",
      latitude: 0.7432,
      longitude: 99.3673
    },
    {
      city: "Kab. Nias",
      latitude: 1.0869,
      longitude: 97.7417
    },
    {
      city: "Kab. Nias barat",
      latitude: 1.0116,
      longitude: 97.4814
    },
    {
      city: "Kab. Nias selatan",
      latitude: 0.7086,
      longitude: 97.8286
    },
    {
      city: "Kab. Nias utara",
      latitude: 1.3166,
      longitude: 97.3949
    },
    {
      city: "Kab. Padang lawas",
      latitude: 1.1187,
      longitude: 99.8125
    },
    {
      city: "Kab. Padang lawas utara",
      latitude: 1.5759,
      longitude: 99.6341
    },
    {
      city: "Kab. Pakpak bharat",
      latitude: 2.5135,
      longitude: 98.2213
    },
    {
      city: "Kab. Samosir",
      latitude: 2.6274,
      longitude: 98.7922
    },
    {
      city: "Kab. Serdang bedagai",
      latitude: 3.3372,
      longitude: 99.0571
    },
    {
      city: "Kab. Simalungun",
      latitude: 2.9782,
      longitude: 99.2786
    },
    {
      city: "Kab. Tapanuli selatan",
      latitude: 1.5775,
      longitude: 99.2786
    },
    {
      city: "Kab. Tapanuli tengah",
      latitude: 1.8493,
      longitude: 98.7041
    },
    {
      city: "Kab. Tapanuli utara",
      latitude: 2.0405,
      longitude: 99.1013
    },
    {
      city: "Kab. Toba samosir",
      latitude: 2.3502,
      longitude: 99.2786
    },
    {
      city: "Kota binjai",
      latitude: 3.6135,
      longitude: 98.5025
    },
    {
      city: "Kota gunungsitoli",
      latitude: 1.2805,
      longitude: 97.6147
    },
    {
      city: "Kota medan",
      latitude: 3.5952,
      longitude: 98.6722
    },
    {
      city: "Kota padangsidempuan",
      latitude: 1.3722,
      longitude: 99.273
    },
    {
      city: "Kota pematangsiantar",
      latitude: 2.97,
      longitude: 99.0682
    },
    {
      city: "Kota sibolga",
      latitude: 1.7368,
      longitude: 98.7851
    },
    {
      city: "Kota tanjungbalai",
      latitude: 2.9662,
      longitude: 99.7985
    },
    {
      city: "Kota tebing tinggi",
      latitude: 3.3263,
      longitude: 99.1567
    },
    {
      city: "Kab. Agam",
      latitude: -0.2209,
      longitude: 100.1703
    },
    {
      city: "Kab. Dharmasraya",
      latitude: -1.1121,
      longitude: 101.6158
    },
    {
      city: "Kab. Kepulauan mentawai",
      latitude: -1.426,
      longitude: 98.9245
    },
    {
      city: "Kab. Lima puluh kota",
      latitude: 0.0734,
      longitude: 100.5296
    },
    {
      city: "Kab. Padang pariaman",
      latitude: -0.5547,
      longitude: 100.2152
    },
    {
      city: "Kab. Pasaman",
      latitude: 0.2209,
      longitude: 100.1703
    },
    {
      city: "Kab. Pasaman barat",
      latitude: 0.2213,
      longitude: 99.6341
    },
    {
      city: "Kab. Pesisir selatan",
      latitude: -1.7223,
      longitude: 100.8903
    },
    {
      city: "Kab. Sijunjung",
      latitude: -0.6647,
      longitude: 101.0712
    },
    {
      city: "Kab. Solok",
      latitude: -0.9644,
      longitude: 100.8903
    },
    {
      city: "Kab. Solok selatan",
      latitude: -1.4157,
      longitude: 101.2524
    },
    {
      city: "Kab. Tanah datar",
      latitude: -0.4797,
      longitude: 100.5746
    },
    {
      city: "Kota bukittinggi",
      latitude: -0.3039,
      longitude: 100.3835
    },
    {
      city: "Kota padang",
      latitude: -0.9471,
      longitude: 100.4172
    },
    {
      city: "Kota padangpanjang",
      latitude: -0.4661,
      longitude: 100.3984
    },
    {
      city: "Kota pariaman",
      latitude: -0.6257,
      longitude: 100.1233
    },
    {
      city: "Kota payakumbuh",
      latitude: -0.2298,
      longitude: 100.6309
    },
    {
      city: "Kota sawahlunto",
      latitude: -0.6841,
      longitude: 100.7323
    },
    {
      city: "Kota solok",
      latitude: -0.7885,
      longitude: 100.655
    },
    {
      city: "Kab. Bengkalis",
      latitude: 1.4139,
      longitude: 101.6158
    },
    {
      city: "Kab. Indragiri hilir",
      latitude: -0.1457,
      longitude: 102.9896
    },
    {
      city: "Kab. Indragiri hulu",
      latitude: -0.7361,
      longitude: 102.2548
    },
    {
      city: "Kab. Kampar",
      latitude: 0.1467,
      longitude: 101.1617
    },
    {
      city: "Kab. Kepulauan meranti",
      latitude: 0.9209,
      longitude: 102.6676
    },
    {
      city: "Kab. Kuantan singingi",
      latitude: -0.4412,
      longitude: 101.5248
    },
    {
      city: "Kab. Pelalawan",
      latitude: 0.1461,
      longitude: 102.2548
    },
    {
      city: "Kab. Rokan hilir",
      latitude: 1.6464,
      longitude: 100.8
    },
    {
      city: "Kab. Rokan hulu",
      latitude: 1.0411,
      longitude: 100.4397
    },
    {
      city: "Kab. Siak",
      latitude: 0.8119,
      longitude: 101.798
    },
    {
      city: "Kota dumai",
      latitude: 1.6666,
      longitude: 101.4002
    },
    {
      city: "Kota pekanbaru",
      latitude: 0.5071,
      longitude: 101.4478
    },
    {
      city: "Kab. Bintan",
      latitude: 1.0619,
      longitude: 104.5189
    },
    {
      city: "Kab. Karimun",
      latitude: 0.7698,
      longitude: 103.4049
    },
    {
      city: "Kab. Kepulauan anambas",
      latitude: 3.1055,
      longitude: 105.6537
    },
    {
      city: "Kab. Lingga",
      latitude: -0.4726,
      longitude: 104.4258
    },
    {
      city: "Kab. Natuna",
      latitude: 4,
      longitude: 108.25
    },
    {
      city: "Kota batam",
      latitude: 1.1301,
      longitude: 104.0529
    },
    {
      city: "Kota tanjung pinang",
      latitude: 0.9186,
      longitude: 104.4665
    },
    {
      city: "Pulau tambelan kab. Bintan",
      latitude: 1.1216,
      longitude: 107.3259
    },
    {
      city: "Pekajang kab. Lingga",
      latitude: -0.0752,
      longitude: 104.6305
    },
    {
      city: "Pulau serasan kab. Natuna",
      latitude: 2.5179,
      longitude: 109.0513
    },
    {
      city: "Pulau midai kab. Natuna",
      latitude: 2.9979,
      longitude: 107.7755
    },
    {
      city: "Pulau laut kab. Natuna",
      latitude: 4.7103,
      longitude: 107.971
    },
    {
      city: "Kab. Batanghari",
      latitude: -1.7084,
      longitude: 103.0818
    },
    {
      city: "Kab. Bungo",
      latitude: -1.6401,
      longitude: 101.8892
    },
    {
      city: "Kab. Kerinci",
      latitude: -1.872,
      longitude: 101.4339
    },
    {
      city: "Kab. Merangin",
      latitude: -2.1753,
      longitude: 101.9805
    },
    {
      city: "Kab. Muaro jambi",
      latitude: -1.5521,
      longitude: 103.8216
    },
    {
      city: "Kab. Sarolangun",
      latitude: -2.323,
      longitude: 102.7135
    },
    {
      city: "Kab. Tanjung jabung barat",
      latitude: -1.1058,
      longitude: 103.0818
    },
    {
      city: "Kab. Tanjung jabung timur",
      latitude: -1.1024,
      longitude: 103.8216
    },
    {
      city: "Kab. Tebo",
      latitude: -1.2593,
      longitude: 102.3464
    },
    {
      city: "Kota jambi",
      latitude: -1.6101,
      longitude: 103.6131
    },
    {
      city: "Kota sungai penuh",
      latitude: -2.0634,
      longitude: 101.3947
    },
    {
      city: "Kab. Bengkulu selatan",
      latitude: -4.3248,
      longitude: 103.0357
    },
    {
      city: "Kab. Bengkulu tengah",
      latitude: -3.6962,
      longitude: 102.3922
    },
    {
      city: "Kab. Bengkulu utara",
      latitude: -3.2663,
      longitude: 101.9805
    },
    {
      city: "Kab. Kaur",
      latitude: -4.5216,
      longitude: 103.2663
    },
    {
      city: "Kab. Kepahiang",
      latitude: -3.613,
      longitude: 102.6676
    },
    {
      city: "Kab. Lebong",
      latitude: -3.1455,
      longitude: 102.209
    },
    {
      city: "Kab. Mukomuko",
      latitude: -2.6449,
      longitude: 101.4339
    },
    {
      city: "Kab. Rejang lebong",
      latitude: -3.4548,
      longitude: 102.6676
    },
    {
      city: "Kab. Seluma",
      latitude: -4.0499,
      longitude: 102.7135
    },
    {
      city: "Kota bengkulu",
      latitude: -3.7928,
      longitude: 102.2608
    },
    {
      city: "Kab. Banyuasin",
      latitude: -2.6096,
      longitude: 104.7521
    },
    {
      city: "Kab. Empat lawang",
      latitude: -3.7286,
      longitude: 102.8975
    },
    {
      city: "Kab. Lahat",
      latitude: -3.8009,
      longitude: 103.3587
    },
    {
      city: "Kab. Muara enim",
      latitude: -3.7114,
      longitude: 104.0072
    },
    {
      city: "Kab. Musi banyuasin",
      latitude: -2.5442,
      longitude: 103.7289
    },
    {
      city: "Kab. Musi rawas",
      latitude: -3.0957,
      longitude: 103.0818
    },
    {
      city: "Kab. Musi rawas utara",
      latitude: -2.7878,
      longitude: 102.7135
    },
    {
      city: "Kab. Ogan ilir",
      latitude: -3.4265,
      longitude: 104.6121
    },
    {
      city: "Kab. Ogan komering ilir",
      latitude: -3.456,
      longitude: 105.2195
    },
    {
      city: "Kab. Ogan komering ulu",
      latitude: -4.0283,
      longitude: 104.0072
    },
    {
      city: "Kab. Ogan komering ulu selatan",
      latitude: -4.6682,
      longitude: 104.0072
    },
    {
      city: "Kab. Ogan komering ulu timur",
      latitude: -3.8568,
      longitude: 104.7521
    },
    {
      city: "Kab. Penukal abab lematang ilir",
      latitude: -3.2398,
      longitude: 104.0072
    },
    {
      city: "Kota lubuklinggau",
      latitude: -3.2996,
      longitude: 102.8572
    },
    {
      city: "Kota pagar alam",
      latitude: -4.042,
      longitude: 103.2279
    },
    {
      city: "Kota palembang",
      latitude: -2.9761,
      longitude: 104.7754
    },
    {
      city: "Kota prabumulih",
      latitude: -3.4214,
      longitude: 104.2437
    },
    {
      city: "Kab. Bangka",
      latitude: -1.8743,
      longitude: 105.923
    },
    {
      city: "Kab. Bangka barat",
      latitude: -1.8405,
      longitude: 105.5005
    },
    {
      city: "Kab. Bangka selatan",
      latitude: -2.7411,
      longitude: 106.4406
    },
    {
      city: "Kab. Bangka tengah",
      latitude: -2.4008,
      longitude: 106.2051
    },
    {
      city: "Kab. Belitung",
      latitude: -2.7217,
      longitude: 107.7636
    },
    {
      city: "Kab. Belitung timur",
      latitude: -2.8678,
      longitude: 108.1429
    },
    {
      city: "Kota pangkal pinang",
      latitude: -2.1316,
      longitude: 106.1169
    },
    {
      city: "Kab. Lampung tengah",
      latitude: -4.8008,
      longitude: 105.3131
    },
    {
      city: "Kab. Lampung utara",
      latitude: -4.8134,
      longitude: 104.7521
    },
    {
      city: "Kab. Lampung selatan",
      latitude: -5.5623,
      longitude: 105.5474
    },
    {
      city: "Kab. Lampung barat",
      latitude: -5.1095,
      longitude: 104.1466
    },
    {
      city: "Kab. Lampung timur",
      latitude: -5.1135,
      longitude: 105.6882
    },
    {
      city: "Kab. Mesuji",
      latitude: -4.0045,
      longitude: 105.3131
    },
    {
      city: "Kab. Pesawaran",
      latitude: -5.4932,
      longitude: 105.0791
    },
    {
      city: "Kab. Pesisir barat",
      latitude: -5.2928,
      longitude: 104.1234
    },
    {
      city: "Kab. Pringsewu",
      latitude: -5.3331,
      longitude: 104.9856
    },
    {
      city: "Kab. Tulang bawang",
      latitude: -4.3177,
      longitude: 105.5005
    },
    {
      city: "Kab. Tulang bawang barat",
      latitude: -4.5257,
      longitude: 105.0791
    },
    {
      city: "Kab. Tanggamus",
      latitude: -5.3027,
      longitude: 104.5655
    },
    {
      city: "Kab. Way kanan",
      latitude: -4.4964,
      longitude: 104.5655
    },
    {
      city: "Kota bandar lampung",
      latitude: -5.3971,
      longitude: 105.2668
    },
    {
      city: "Kota metro",
      latitude: -5.1178,
      longitude: 105.3073
    },
    {
      city: "Kab. Lebak",
      latitude: -6.5644,
      longitude: 106.2522
    },
    {
      city: "Kab. Pandeglang",
      latitude: -6.7483,
      longitude: 105.6882
    },
    {
      city: "Kab. Serang",
      latitude: -6.1397,
      longitude: 106.0405
    },
    {
      city: "Kab. Tangerang",
      latitude: -6.1872,
      longitude: 106.4877
    },
    {
      city: "Kota cilegon",
      latitude: -6.0025,
      longitude: 106.0111
    },
    {
      city: "Kota serang",
      latitude: -6.1104,
      longitude: 106.164
    },
    {
      city: "Kota tangerang",
      latitude: -6.2024,
      longitude: 106.6527
    },
    {
      city: "Kota tangerang selatan",
      latitude: -6.2835,
      longitude: 106.7113
    },
    {
      city: "Kab. Bandung",
      latitude: -7.1341,
      longitude: 107.6215
    },
    {
      city: "Kab. Bandung barat",
      latitude: -6.8652,
      longitude: 107.492
    },
    {
      city: "Kab. Bekasi",
      latitude: -6.2474,
      longitude: 107.1485
    },
    {
      city: "Kab. Bogor",
      latitude: -6.5518,
      longitude: 106.6291
    },
    {
      city: "Kab. Ciamis",
      latitude: -7.3321,
      longitude: 108.3493
    },
    {
      city: "Kab. Cianjur",
      latitude: -7.358,
      longitude: 107.1957
    },
    {
      city: "Kab. Cirebon",
      latitude: -6.6899,
      longitude: 108.4751
    },
    {
      city: "Kab. Garut",
      latitude: -7.5012,
      longitude: 107.7636
    },
    {
      city: "Kab. Indramayu",
      latitude: -6.3373,
      longitude: 108.3258
    },
    {
      city: "Kab. Karawang",
      latitude: -6.3227,
      longitude: 107.3376
    },
    {
      city: "Kab. Kuningan",
      latitude: -7.0138,
      longitude: 108.5701
    },
    {
      city: "Kab. Majalengka",
      latitude: -6.7791,
      longitude: 108.2852
    },
    {
      city: "Kab. Pangandaran",
      latitude: -7.6151,
      longitude: 108.4988
    },
    {
      city: "Kab. Purwakarta",
      latitude: -6.5649,
      longitude: 107.4322
    },
    {
      city: "Kab. Subang",
      latitude: -6.3488,
      longitude: 107.7636
    },
    {
      city: "Kab. Sukabumi",
      latitude: -6.8649,
      longitude: 106.9536
    },
    {
      city: "Kab. Sumedang",
      latitude: -6.8329,
      longitude: 107.9532
    },
    {
      city: "Kab. Tasikmalaya",
      latitude: -7.6513,
      longitude: 108.1429
    },
    {
      city: "Kota bandung",
      latitude: -6.9175,
      longitude: 107.6191
    },
    {
      city: "Kota banjar",
      latitude: -7.3707,
      longitude: 108.5342
    },
    {
      city: "Kota bekasi",
      latitude: -6.2383,
      longitude: 106.9756
    },
    {
      city: "Kota bogor",
      latitude: -6.5971,
      longitude: 106.806
    },
    {
      city: "Kota cimahi",
      latitude: -6.8841,
      longitude: 107.5413
    },
    {
      city: "Kota cirebon",
      latitude: -6.732,
      longitude: 108.5523
    },
    {
      city: "Kota depok",
      latitude: -6.4025,
      longitude: 106.7942
    },
    {
      city: "Kota sukabumi",
      latitude: -6.9277,
      longitude: 106.93
    },
    {
      city: "Kota tasikmalaya",
      latitude: -7.3506,
      longitude: 108.2172
    },
    {
      city: "Kota jakarta",
      latitude: -6.1751,
      longitude: 106.865
    },
    {
      city: "Kab. Administrasi kepulauan seribu",
      latitude: -5.6122,
      longitude: 106.617
    },
    {
      city: "Kab. Banjarnegara",
      latitude: -7.3794,
      longitude: 109.6163
    },
    {
      city: "Kab. Banyumas",
      latitude: -7.4832,
      longitude: 109.1404
    },
    {
      city: "Kab. Batang",
      latitude: -7.0392,
      longitude: 109.9021
    },
    {
      city: "Kab. Blora",
      latitude: -7.0122,
      longitude: 111.3799
    },
    {
      city: "Kab. Boyolali",
      latitude: -7.4318,
      longitude: 110.6884
    },
    {
      city: "Kab. Brebes",
      latitude: -6.9592,
      longitude: 108.9027
    },
    {
      city: "Kab. Cilacap",
      latitude: -7.6178,
      longitude: 108.9027
    },
    {
      city: "Kab. Demak",
      latitude: -6.9239,
      longitude: 110.6646
    },
    {
      city: "Kab. Grobogan",
      latitude: -7.1542,
      longitude: 110.9507
    },
    {
      city: "Kab. Jepara",
      latitude: -6.5827,
      longitude: 110.6787
    },
    {
      city: "Kab. Karanganyar",
      latitude: -7.6387,
      longitude: 111.046
    },
    {
      city: "Kab. Kebumen",
      latitude: -7.6681,
      longitude: 109.6525
    },
    {
      city: "Kab. Kendal",
      latitude: -7.0265,
      longitude: 110.1879
    },
    {
      city: "Kab. Klaten",
      latitude: -7.6579,
      longitude: 110.6646
    },
    {
      city: "Kab. Kudus",
      latitude: -6.7726,
      longitude: 110.8791
    },
    {
      city: "Kab. Magelang",
      latitude: -7.4305,
      longitude: 110.2832
    },
    {
      city: "Kab. Pati",
      latitude: -6.745,
      longitude: 111.046
    },
    {
      city: "Kab. Pekalongan",
      latitude: -7.0517,
      longitude: 109.6163
    },
    {
      city: "Kab. Pemalang",
      latitude: -7.0599,
      longitude: 109.4259
    },
    {
      city: "Kab. Purbalingga",
      latitude: -7.3059,
      longitude: 109.4259
    },
    {
      city: "Kab. Purworejo",
      latitude: -7.6965,
      longitude: 109.9989
    },
    {
      city: "Kab. Rembang",
      latitude: -6.8082,
      longitude: 111.4276
    },
    {
      city: "Kab. Semarang",
      latitude: -7.1765,
      longitude: 110.4739
    },
    {
      city: "Kab. Sragen",
      latitude: -7.4303,
      longitude: 111.0092
    },
    {
      city: "Kab. Sukoharjo",
      latitude: -7.6484,
      longitude: 110.8553
    },
    {
      city: "Kab. Tegal",
      latitude: -6.8588,
      longitude: 109.1048
    },
    {
      city: "Kab. Temanggung",
      latitude: -7.2749,
      longitude: 110.0892
    },
    {
      city: "Kab. Wonogiri",
      latitude: -7.8846,
      longitude: 111.046
    },
    {
      city: "Kab. Wonosobo",
      latitude: -7.3632,
      longitude: 109.9002
    },
    {
      city: "Kota magelang",
      latitude: -7.4797,
      longitude: 110.2177
    },
    {
      city: "Kota pekalongan",
      latitude: -6.8898,
      longitude: 109.6746
    },
    {
      city: "Kota salatiga",
      latitude: -7.3305,
      longitude: 110.5084
    },
    {
      city: "Kota semarang",
      latitude: -7.0051,
      longitude: 110.4381
    },
    {
      city: "Kota surakarta",
      latitude: -7.5755,
      longitude: 110.8243
    },
    {
      city: "Kota tegal",
      latitude: -6.8797,
      longitude: 109.1256
    },
    {
      city: "Kab. Bantul",
      latitude: -7.919,
      longitude: 110.3785
    },
    {
      city: "Kab. Gunungkidul",
      latitude: -8.0305,
      longitude: 110.6169
    },
    {
      city: "Kab. Kulon progo",
      latitude: -7.8267,
      longitude: 110.1641
    },
    {
      city: "Kab. Sleman",
      latitude: -7.7325,
      longitude: 110.4024
    },
    {
      city: "Kota yogyakarta",
      latitude: -7.7956,
      longitude: 110.3695
    },
    {
      city: "Kab. Bangkalan",
      latitude: -7.0384,
      longitude: 112.9137
    },
    {
      city: "Kab. Banyuwangi",
      latitude: -8.2191,
      longitude: 114.3691
    },
    {
      city: "Kab. Blitar",
      latitude: -8.0955,
      longitude: 112.1609
    },
    {
      city: "Kab. Bojonegoro",
      latitude: -7.3175,
      longitude: 111.7615
    },
    {
      city: "Kab. Bondowoso",
      latitude: -7.9674,
      longitude: 113.9061
    },
    {
      city: "Kab. Gresik",
      latitude: -7.155,
      longitude: 112.5722
    },
    {
      city: "Kab. Jember",
      latitude: -8.1845,
      longitude: 113.6681
    },
    {
      city: "Kab. Jombang",
      latitude: -7.5741,
      longitude: 112.2861
    },
    {
      city: "Kab. Kediri",
      latitude: -7.8232,
      longitude: 112.1907
    },
    {
      city: "Kab. Lamongan",
      latitude: -7.1269,
      longitude: 112.3338
    },
    {
      city: "Kab. Lumajang",
      latitude: -8.0944,
      longitude: 113.1442
    },
    {
      city: "Kab. Madiun",
      latitude: -7.6093,
      longitude: 111.6184
    },
    {
      city: "Kab. Magetan",
      latitude: -7.6433,
      longitude: 111.356
    },
    {
      city: "Kab. Malang",
      latitude: -8.2422,
      longitude: 112.7152
    },
    {
      city: "Kab. Mojokerto",
      latitude: -7.4699,
      longitude: 112.4351
    },
    {
      city: "Kab. Nganjuk",
      latitude: -7.5944,
      longitude: 111.9046
    },
    {
      city: "Kab. Ngawi",
      latitude: -7.461,
      longitude: 111.3322
    },
    {
      city: "Kab. Pacitan",
      latitude: -8.1263,
      longitude: 111.1414
    },
    {
      city: "Kab. Pamekasan",
      latitude: -7.1051,
      longitude: 113.5252
    },
    {
      city: "Kab. Pasuruan",
      latitude: -7.786,
      longitude: 112.8582
    },
    {
      city: "Kab. Ponorogo",
      latitude: -7.8651,
      longitude: 111.4696
    },
    {
      city: "Kab. Probolinggo",
      latitude: -7.8718,
      longitude: 113.4776
    },
    {
      city: "Kab. Sampang",
      latitude: -7.0402,
      longitude: 113.2394
    },
    {
      city: "Kab. Sidoarjo",
      latitude: -7.4726,
      longitude: 112.6675
    },
    {
      city: "Kab. Situbondo",
      latitude: -7.7889,
      longitude: 114.1915
    },
    {
      city: "Kab. Sumenep",
      latitude: -6.9254,
      longitude: 113.9061
    },
    {
      city: "Kab. Trenggalek",
      latitude: -8.1824,
      longitude: 111.6184
    },
    {
      city: "Kab. Tuban",
      latitude: -6.8955,
      longitude: 112.0298
    },
    {
      city: "Kab. Tulungagung",
      latitude: -8.0912,
      longitude: 111.9642
    },
    {
      city: "Kota batu",
      latitude: -7.8831,
      longitude: 112.5334
    },
    {
      city: "Kota blitar",
      latitude: -8.0955,
      longitude: 112.1609
    },
    {
      city: "Kota kediri",
      latitude: -7.848,
      longitude: 112.0178
    },
    {
      city: "Kota madiun",
      latitude: -7.6311,
      longitude: 111.53
    },
    {
      city: "Kota malang",
      latitude: -7.9666,
      longitude: 112.6326
    },
    {
      city: "Kota mojokerto",
      latitude: -7.4705,
      longitude: 112.4401
    },
    {
      city: "Kota pasuruan",
      latitude: -7.6469,
      longitude: 112.8999
    },
    {
      city: "Kota probolinggo",
      latitude: -7.7764,
      longitude: 113.2037
    },
    {
      city: "Kota surabaya",
      latitude: -7.2575,
      longitude: 112.7521
    },
    {
      city: "Kab. Badung",
      latitude: -8.5819,
      longitude: 115.1771
    },
    {
      city: "Kab. Bangli",
      latitude: -8.2976,
      longitude: 115.3549
    },
    {
      city: "Kab. Buleleng",
      latitude: -8.2239,
      longitude: 114.9517
    },
    {
      city: "Kab. Gianyar",
      latitude: -8.4248,
      longitude: 115.2601
    },
    {
      city: "Kab. Jembrana",
      latitude: -8.3233,
      longitude: 114.6668
    },
    {
      city: "Kab. Karangasem",
      latitude: -8.3466,
      longitude: 115.5207
    },
    {
      city: "Kab. Klungkung",
      latitude: -8.7278,
      longitude: 115.5444
    },
    {
      city: "Kab. Tabanan",
      latitude: -8.4596,
      longitude: 115.0466
    },
    {
      city: "Kota denpasar",
      latitude: -8.6705,
      longitude: 115.2126
    },
    {
      city: "Kab. Bima",
      latitude: -8.4354,
      longitude: 118.6265
    },
    {
      city: "Kab. Dompu",
      latitude: -8.5364,
      longitude: 118.3462
    },
    {
      city: "Kab. Lombok barat",
      latitude: -8.6465,
      longitude: 116.1123
    },
    {
      city: "Kab. Lombok tengah",
      latitude: -8.6946,
      longitude: 116.2777
    },
    {
      city: "Kab. Lombok timur",
      latitude: -8.5134,
      longitude: 116.561
    },
    {
      city: "Kab. Lombok utara",
      latitude: -8.3739,
      longitude: 116.2777
    },
    {
      city: "Kab. Sumbawa",
      latitude: -8.6529,
      longitude: 117.3616
    },
    {
      city: "Kab. Sumbawa barat",
      latitude: -8.9293,
      longitude: 116.891
    },
    {
      city: "Kota bima",
      latitude: -8.4643,
      longitude: 118.7449
    },
    {
      city: "Kota mataram",
      latitude: -8.577,
      longitude: 116.1005
    },
    {
      city: "Kab. Alor",
      latitude: -8.2928,
      longitude: 124.5528
    },
    {
      city: "Kab. Belu",
      latitude: -9.1539,
      longitude: 124.9066
    },
    {
      city: "Kab. Ende",
      latitude: -8.6763,
      longitude: 121.7195
    },
    {
      city: "Kab. Flores timur",
      latitude: -8.3131,
      longitude: 122.9663
    },
    {
      city: "Kab. Kupang",
      latitude: -9.9906,
      longitude: 123.8858
    },
    {
      city: "Kab. Lembata",
      latitude: -8.4719,
      longitude: 123.4832
    },
    {
      city: "Kab. Malaka",
      latitude: -9.5309,
      longitude: 124.9066
    },
    {
      city: "Kab. Manggarai",
      latitude: -8.6797,
      longitude: 120.3897
    },
    {
      city: "Kab. Manggarai barat",
      latitude: -8.6688,
      longitude: 120.0665
    },
    {
      city: "Kab. Manggarai timur",
      latitude: -8.6207,
      longitude: 120.62
    },
    {
      city: "Kab. Ngada",
      latitude: -8.743,
      longitude: 120.9876
    },
    {
      city: "Kab. Nagekeo",
      latitude: -8.6754,
      longitude: 121.3084
    },
    {
      city: "Kab. Rote ndao",
      latitude: -10.7386,
      longitude: 123.1239
    },
    {
      city: "Kab. Sabu raijua",
      latitude: -10.5541,
      longitude: 121.8335
    },
    {
      city: "Kab. Sikka",
      latitude: -8.6766,
      longitude: 122.1292
    },
    {
      city: "Kab. Sumba barat",
      latitude: -9.6548,
      longitude: 119.3947
    },
    {
      city: "Kab. Sumba barat daya",
      latitude: -9.5391,
      longitude: 119.1391
    },
    {
      city: "Kab. Sumba tengah",
      latitude: -9.4879,
      longitude: 119.6963
    },
    {
      city: "Kab. Sumba timur",
      latitude: -9.9802,
      longitude: 120.3436
    },
    {
      city: "Kab. Timor tengah selatan",
      latitude: -9.7763,
      longitude: 124.4198
    },
    {
      city: "Kab. Timor tengah utara",
      latitude: -9.4523,
      longitude: 124.5971
    },
    {
      city: "Kota kupang",
      latitude: -10.1772,
      longitude: 123.607
    },
    {
      city: "Kab. Bengkayang",
      latitude: 1.0691,
      longitude: 109.6639
    },
    {
      city: "Kab. Kapuas hulu",
      latitude: 0.8337,
      longitude: 113.0012
    },
    {
      city: "Kab. Kayong utara",
      latitude: -0.9226,
      longitude: 110.045
    },
    {
      city: "Kab. Ketapang",
      latitude: -1.5698,
      longitude: 110.5215
    },
    {
      city: "Kab. Kubu raya",
      latitude: -0.3534,
      longitude: 109.4735
    },
    {
      city: "Kab. Landak",
      latitude: 0.4237,
      longitude: 109.7592
    },
    {
      city: "Kab. Melawi",
      latitude: -0.7001,
      longitude: 111.6661
    },
    {
      city: "Kab. Mempawah",
      latitude: 0.3897,
      longitude: 109.1404
    },
    {
      city: "Kab. Sambas",
      latitude: 1.3625,
      longitude: 109.2832
    },
    {
      city: "Kab. Sanggau",
      latitude: 0.14,
      longitude: 110.5215
    },
    {
      city: "Kab. Sekadau",
      latitude: -0.0697,
      longitude: 110.9984
    },
    {
      city: "Kab. Sintang",
      latitude: -0.1378,
      longitude: 112.8106
    },
    {
      city: "Kota pontianak",
      latitude: -0.0263,
      longitude: 109.3425
    },
    {
      city: "Kota singkawang",
      latitude: 0.906,
      longitude: 108.9872
    },
    {
      city: "Kab. Balangan",
      latitude: -2.326,
      longitude: 115.6155
    },
    {
      city: "Kab. Banjar",
      latitude: -3.32,
      longitude: 114.9991
    },
    {
      city: "Kab. Barito kuala",
      latitude: -3.0715,
      longitude: 114.6668
    },
    {
      city: "Kab. Hulu sungai selatan",
      latitude: -2.7663,
      longitude: 115.2363
    },
    {
      city: "Kab. Hulu sungai tengah",
      latitude: -2.6153,
      longitude: 115.5207
    },
    {
      city: "Kab. Hulu sungai utara",
      latitude: -2.4421,
      longitude: 115.1889
    },
    {
      city: "Kab. Kotabaru",
      latitude: -3.003,
      longitude: 115.9468
    },
    {
      city: "Kab. Tabalong",
      latitude: -1.8643,
      longitude: 115.5681
    },
    {
      city: "Kab. Tanah bumbu",
      latitude: -3.4512,
      longitude: 115.5681
    },
    {
      city: "Kab. Tanah laut",
      latitude: -3.7694,
      longitude: 114.8093
    },
    {
      city: "Kab. Tapin",
      latitude: -2.9161,
      longitude: 115.0466
    },
    {
      city: "Kota banjarbaru",
      latitude: -3.4572,
      longitude: 114.8103
    },
    {
      city: "Kota banjarmasin",
      latitude: -3.3186,
      longitude: 114.5944
    },
    {
      city: "Kab. Barito selatan",
      latitude: -1.8759,
      longitude: 114.8093
    },
    {
      city: "Kab. Barito timur",
      latitude: -2.0124,
      longitude: 115.1889
    },
    {
      city: "Kab. Barito utara",
      latitude: -0.9587,
      longitude: 115.094
    },
    {
      city: "Kab. Gunung mas",
      latitude: -1.2522,
      longitude: 113.5729
    },
    {
      city: "Kab. Kapuas",
      latitude: -1.8116,
      longitude: 114.3341
    },
    {
      city: "Kab. Katingan",
      latitude: -0.9758,
      longitude: 112.8106
    },
    {
      city: "Kab. Kotawaringin barat",
      latitude: -2.5063,
      longitude: 111.7615
    },
    {
      city: "Kab. Kotawaringin timur",
      latitude: -2.1225,
      longitude: 112.8106
    },
    {
      city: "Kab. Lamandau",
      latitude: -1.8526,
      longitude: 111.2845
    },
    {
      city: "Kab. Murung raya",
      latitude: -0.1362,
      longitude: 114.3341
    },
    {
      city: "Kab. Pulang pisau",
      latitude: -2.685,
      longitude: 113.9536
    },
    {
      city: "Kab. Sukamara",
      latitude: -2.6268,
      longitude: 111.2368
    },
    {
      city: "Kab. Seruyan",
      latitude: -3.0123,
      longitude: 112.4291
    },
    {
      city: "Kota palangkaraya",
      latitude: -2.2161,
      longitude: 113.914
    },
    {
      city: "Kab. Berau",
      latitude: 2.0451,
      longitude: 117.3616
    },
    {
      city: "Kab. Kutai barat",
      latitude: -0.4052,
      longitude: 115.8522
    },
    {
      city: "Kab. Kutai kartanegara",
      latitude: -0.1337,
      longitude: 116.6082
    },
    {
      city: "Kab. Kutai timur",
      latitude: 0.9434,
      longitude: 116.9852
    },
    {
      city: "Kab. Mahakam ulu",
      latitude: 0.9617,
      longitude: 114.7143
    },
    {
      city: "Kab. Paser",
      latitude: -1.7175,
      longitude: 115.9468
    },
    {
      city: "Kab. Penajam paser utara",
      latitude: -1.2917,
      longitude: 116.5138
    },
    {
      city: "Kota balikpapan",
      latitude: -1.2379,
      longitude: 116.8529
    },
    {
      city: "Kota bontang",
      latitude: 0.1209,
      longitude: 117.48
    },
    {
      city: "Kota samarinda",
      latitude: -0.4948,
      longitude: 117.1436
    },
    {
      city: "Kab. Bulungan",
      latitude: 2.9042,
      longitude: 116.9852
    },
    {
      city: "Kab. Malinau",
      latitude: 3.0731,
      longitude: 116.0414
    },
    {
      city: "Kab. Nunukan",
      latitude: 4.081,
      longitude: 116.6082
    },
    {
      city: "Kab. Tana tidung",
      latitude: 3.5519,
      longitude: 117.0794
    },
    {
      city: "Kota tarakan",
      latitude: 3.3274,
      longitude: 117.5785
    },
    {
      city: "Kab. Boalemo",
      latitude: 0.7013,
      longitude: 122.2654
    },
    {
      city: "Kab. Bone bolango",
      latitude: 0.5658,
      longitude: 123.3486
    },
    {
      city: "Kab. Gorontalo",
      latitude: 0.5693,
      longitude: 122.8084
    },
    {
      city: "Kab. Gorontalo utara",
      latitude: 0.9253,
      longitude: 122.492
    },
    {
      city: "Kab. Pohuwato",
      latitude: 0.7055,
      longitude: 121.7195
    },
    {
      city: "Kota gorontalo",
      latitude: 0.5435,
      longitude: 123.0568
    },
    {
      city: "Kab. Bantaeng",
      latitude: -5.5169,
      longitude: 120.0203
    },
    {
      city: "Kab. Barru",
      latitude: -4.4364,
      longitude: 119.6499
    },
    {
      city: "Kab. Bone",
      latitude: -4.7443,
      longitude: 120.0665
    },
    {
      city: "Kab. Bulukumba",
      latitude: -5.4329,
      longitude: 120.2051
    },
    {
      city: "Kab. Enrekang",
      latitude: -3.4591,
      longitude: 119.8815
    },
    {
      city: "Kab. Gowa",
      latitude: -5.3103,
      longitude: 119.7426
    },
    {
      city: "Kab. Jeneponto",
      latitude: -5.5546,
      longitude: 119.6731
    },
    {
      city: "Kab. Kepulauan selayar",
      latitude: -6.287,
      longitude: 120.5049
    },
    {
      city: "Kab. Luwu",
      latitude: -3.3052,
      longitude: 120.2513
    },
    {
      city: "Kab. Luwu timur",
      latitude: -2.5826,
      longitude: 121.171
    },
    {
      city: "Kab. Luwu utara",
      latitude: -2.269,
      longitude: 119.9741
    },
    {
      city: "Kab. Maros",
      latitude: -5.0549,
      longitude: 119.6963
    },
    {
      city: "Kab. Pangkajene dan kepulauan",
      latitude: -4.805,
      longitude: 119.5572
    },
    {
      city: "Kab. Pinrang",
      latitude: -3.6483,
      longitude: 119.5572
    },
    {
      city: "Kab. Sidenreng rappang",
      latitude: -3.7739,
      longitude: 120.0203
    },
    {
      city: "Kab. Sinjai",
      latitude: -5.2172,
      longitude: 120.1127
    },
    {
      city: "Kab. Soppeng",
      latitude: -4.3519,
      longitude: 119.9278
    },
    {
      city: "Kab. Takalar",
      latitude: -5.4162,
      longitude: 119.4876
    },
    {
      city: "Kab. Tana toraja",
      latitude: -3.0753,
      longitude: 119.7426
    },
    {
      city: "Kab. Toraja utara",
      latitude: -2.8622,
      longitude: 119.8352
    },
    {
      city: "Kab. Wajo",
      latitude: -4.0222,
      longitude: 120.0665
    },
    {
      city: "Kota makassar",
      latitude: -5.1477,
      longitude: 119.4327
    },
    {
      city: "Kota palopo",
      latitude: -3.0016,
      longitude: 120.1985
    },
    {
      city: "Kota parepare",
      latitude: -4.0096,
      longitude: 119.6291
    },
    {
      city: "Kab. Bombana",
      latitude: -4.6543,
      longitude: 121.9018
    },
    {
      city: "Kab. Buton",
      latitude: -5.3096,
      longitude: 122.9888
    },
    {
      city: "Kab. Buton selatan",
      latitude: -5.3096,
      longitude: 122.9888
    },
    {
      city: "Kab. Buton tengah",
      latitude: -5.3891,
      longitude: 122.5599
    },
    {
      city: "Kab. Buton utara",
      latitude: -4.7023,
      longitude: 123.0339
    },
    {
      city: "Kab. Kolaka",
      latitude: -3.9947,
      longitude: 121.5827
    },
    {
      city: "Kab. Kolaka timur",
      latitude: -4.2279,
      longitude: 121.9018
    },
    {
      city: "Kab. Kolaka utara",
      latitude: -3.1347,
      longitude: 121.171
    },
    {
      city: "Kab. Konawe",
      latitude: -3.938,
      longitude: 122.0837
    },
    {
      city: "Kab. Konawe kepulauan",
      latitude: -4.1361,
      longitude: 123.1239
    },
    {
      city: "Kab. Konawe selatan",
      latitude: -4.2028,
      longitude: 122.4467
    },
    {
      city: "Kab. Konawe utara",
      latitude: -3.3803,
      longitude: 122.0837
    },
    {
      city: "Kab. Muna",
      latitude: -4.9016,
      longitude: 122.6277
    },
    {
      city: "Kab. Muna barat",
      latitude: -4.9016,
      longitude: 122.6277
    },
    {
      city: "Kab. Wakatobi",
      latitude: -5.6504,
      longitude: 123.8902
    },
    {
      city: "Kota bau-bau",
      latitude: -5.5071,
      longitude: 122.5969
    },
    {
      city: "Kota kendari",
      latitude: -3.9985,
      longitude: 122.513
    },
    {
      city: "Kab. Banggai",
      latitude: -0.9562,
      longitude: 122.6277
    },
    {
      city: "Kab. Banggai kepulauan",
      latitude: -0.9562,
      longitude: 122.6277
    },
    {
      city: "Kab. Banggai laut",
      latitude: -1.6735,
      longitude: 123.5504
    },
    {
      city: "Kab. Buol",
      latitude: 0.9695,
      longitude: 121.3542
    },
    {
      city: "Kab. Donggala",
      latitude: -0.4233,
      longitude: 119.8352
    },
    {
      city: "Kab. Morowali",
      latitude: -2.6987,
      longitude: 121.9018
    },
    {
      city: "Kab. Morowali utara",
      latitude: -1.6312,
      longitude: 121.3542
    },
    {
      city: "Kab. Parigi moutong",
      latitude: 0.5818,
      longitude: 120.8039
    },
    {
      city: "Kab. Poso",
      latitude: -1.6469,
      longitude: 120.4358
    },
    {
      city: "Kab. Sigi",
      latitude: -1.386,
      longitude: 119.8815
    },
    {
      city: "Kab. Tojo una-una",
      latitude: -1.0988,
      longitude: 121.537
    },
    {
      city: "Kab. Toli-toli",
      latitude: 0.8768,
      longitude: 120.758
    },
    {
      city: "Kota palu",
      latitude: -0.9003,
      longitude: 119.878
    },
    {
      city: "Kab. Bolaang mongondow",
      latitude: 0.6871,
      longitude: 124.0641
    },
    {
      city: "Kab. Bolaang mongondow selatan",
      latitude: 0.4053,
      longitude: 123.8411
    },
    {
      city: "Kab. Bolaang mongondow timur",
      latitude: 0.7153,
      longitude: 124.4642
    },
    {
      city: "Kab. Bolaang mongondow utara",
      latitude: 0.907,
      longitude: 123.2657
    },
    {
      city: "Kab. Kepulauan sangihe",
      latitude: 3.6329,
      longitude: 125.5001
    },
    {
      city: "Kab. Kepulauan siau tagulandang biaro",
      latitude: 2.346,
      longitude: 125.4124
    },
    {
      city: "Kab. Kepulauan talaud",
      latitude: 4.3067,
      longitude: 126.8035
    },
    {
      city: "Kab. Minahasa",
      latitude: 1.2169,
      longitude: 124.8183
    },
    {
      city: "Kab. Minahasa selatan",
      latitude: 1.0947,
      longitude: 124.4642
    },
    {
      city: "Kab. Minahasa tenggara",
      latitude: 1.0279,
      longitude: 124.7299
    },
    {
      city: "Kab. Minahasa utara",
      latitude: 1.5328,
      longitude: 124.9948
    },
    {
      city: "Kota bitung",
      latitude: 1.4404,
      longitude: 125.1217
    },
    {
      city: "Kota kotamobagu",
      latitude: 0.7244,
      longitude: 124.3199
    },
    {
      city: "Kota manado",
      latitude: 1.4748,
      longitude: 124.8421
    },
    {
      city: "Kota tomohon",
      latitude: 1.3229,
      longitude: 124.8405
    },
    {
      city: "Kab. Majene",
      latitude: -3.0297,
      longitude: 118.9063
    },
    {
      city: "Kab. Mamasa",
      latitude: -2.9118,
      longitude: 119.325
    },
    {
      city: "Kab. Mamuju",
      latitude: -2.492,
      longitude: 119.325
    },
    {
      city: "Kab. Mamuju tengah",
      latitude: -1.9354,
      longitude: 119.5108
    },
    {
      city: "Kab. Mamuju utara",
      latitude: -1.5265,
      longitude: 119.5108
    },
    {
      city: "Kab. Polewali mandar",
      latitude: -3.3419,
      longitude: 119.1391
    },
    {
      city: "Kab. Buru",
      latitude: -3.3307,
      longitude: 126.6957
    },
    {
      city: "Kab. Buru selatan",
      latitude: -3.7274,
      longitude: 126.6957
    },
    {
      city: "Kab. Kepulauan aru",
      latitude: -6.1947,
      longitude: 134.5502
    },
    {
      city: "Kab. Maluku barat daya",
      latitude: -7.7852,
      longitude: 126.3498
    },
    {
      city: "Kab. Maluku tengah",
      latitude: -3.0167,
      longitude: 129.4864
    },
    {
      city: "Kab. Maluku tenggara",
      latitude: -5.7512,
      longitude: 132.7272
    },
    {
      city: "Kab. Maluku tenggara barat",
      latitude: -7.5323,
      longitude: 131.3611
    },
    {
      city: "Kab. Seram bagian barat",
      latitude: -3.1272,
      longitude: 128.4008
    },
    {
      city: "Kab. Seram bagian timur",
      latitude: -3.4233,
      longitude: 130.2271
    },
    {
      city: "Kota ambon",
      latitude: -3.6554,
      longitude: 128.1908
    },
    {
      city: "Kota tual",
      latitude: -5.6266,
      longitude: 132.7521
    },
    {
      city: "Kab. Halmahera barat",
      latitude: 1.359,
      longitude: 127.5961
    },
    {
      city: "Kab. Halmahera tengah",
      latitude: 0.442,
      longitude: 128.3587
    },
    {
      city: "Kab. Halmahera utara",
      latitude: 1.5074,
      longitude: 127.8937
    },
    {
      city: "Kab. Halmahera selatan",
      latitude: -1.5109,
      longitude: 127.7238
    },
    {
      city: "Kab. Kepulauan sula",
      latitude: -1.8456,
      longitude: 125.3431
    },
    {
      city: "Kab. Halmahera timur",
      latitude: 1.3121,
      longitude: 128.485
    },
    {
      city: "Kab. Pulau morotai",
      latitude: 2.3657,
      longitude: 128.4008
    },
    {
      city: "Kab. Pulau taliabu",
      latitude: -1.8268,
      longitude: 124.7741
    },
    {
      city: "Kota ternate",
      latitude: 0.771,
      longitude: 127.3695
    },
    {
      city: "Kota tidore kepulauan",
      latitude: 0.674,
      longitude: 127.4041
    },
    {
      city: "Kab. Asmat",
      latitude: -5.0574,
      longitude: 138.3988
    },
    {
      city: "Kab. Biak numfor",
      latitude: -1.0381,
      longitude: 135.9801
    },
    {
      city: "Kab. Boven digoel",
      latitude: -5.74,
      longitude: 140.3482
    },
    {
      city: "Kab. Deiyai",
      latitude: -4.0975,
      longitude: 136.4393
    },
    {
      city: "Kab. Dogiyai",
      latitude: -4.0454,
      longitude: 135.6763
    },
    {
      city: "Kab. Intan jaya",
      latitude: -3.5076,
      longitude: 136.7478
    },
    {
      city: "Kab. Jayapura",
      latitude: -2.9879,
      longitude: 139.8547
    },
    {
      city: "Kab. Jayawijaya",
      latitude: -4.0004,
      longitude: 138.7995
    },
    {
      city: "Kab. Keerom",
      latitude: -3.345,
      longitude: 140.7624
    },
    {
      city: "Kab. Kepulauan yapen",
      latitude: -1.7469,
      longitude: 136.1709
    },
    {
      city: "Kab. Lanny jaya",
      latitude: -3.971,
      longitude: 138.319
    },
    {
      city: "Kab. Mamberamo raya",
      latitude: -2.5331,
      longitude: 137.7638
    },
    {
      city: "Kab. Mamberamo tengah",
      latitude: -2.3746,
      longitude: 138.319
    },
    {
      city: "Kab. Mappi",
      latitude: -6.7606,
      longitude: 139.6911
    },
    {
      city: "Kab. Merauke",
      latitude: -7.7838,
      longitude: 139.0413
    },
    {
      city: "Kab. Mimika",
      latitude: -4.4553,
      longitude: 137.1362
    },
    {
      city: "Kab. Nabire",
      latitude: -3.5095,
      longitude: 135.7521
    },
    {
      city: "Kab. Nduga",
      latitude: -4.4069,
      longitude: 138.2394
    },
    {
      city: "Kab. Paniai",
      latitude: -3.7876,
      longitude: 136.3625
    },
    {
      city: "Kab. Pegunungan bintang",
      latitude: -4.559,
      longitude: 140.5136
    },
    {
      city: "Kab. Puncak",
      latitude: -3.8649,
      longitude: 137.6062
    },
    {
      city: "Kab. Puncak jaya",
      latitude: -3.4468,
      longitude: 137.8427
    },
    {
      city: "Kab. Sarmi",
      latitude: -2.4678,
      longitude: 139.2031
    },
    {
      city: "Kab. Supiori",
      latitude: -0.7295,
      longitude: 135.6385
    },
    {
      city: "Kab. Tolikara",
      latitude: -3.4811,
      longitude: 138.4787
    },
    {
      city: "Kab. Waropen",
      latitude: -1.7469,
      longitude: 136.1709
    },
    {
      city: "Kab. Yahukimo",
      latitude: -4.494,
      longitude: 139.528
    },
    {
      city: "Kab. Yalimo",
      latitude: -3.7853,
      longitude: 139.4466
    },
    {
      city: "Kota jayapura",
      latitude: -2.5916,
      longitude: 140.669
    },
    {
      city: "Kab. Yapen waropen",
      latitude: -1.7469,
      longitude: 136.1709
    },
    {
      city: "Kab. Fakfak",
      latitude: -3.0977,
      longitude: 133.0195
    },
    {
      city: "Kab. Kaimana",
      latitude: -3.2884,
      longitude: 133.9437
    },
    {
      city: "Kab. Manokwari",
      latitude: -0.999,
      longitude: 133.0195
    },
    {
      city: "Kab. Manokwari selatan",
      latitude: -0.999,
      longitude: 133.0195
    },
    {
      city: "Kab. Maybrat",
      latitude: -1.2971,
      longitude: 132.3151
    },
    {
      city: "Kab. Pegunungan arfak",
      latitude: -1.1555,
      longitude: 133.7142
    },
    {
      city: "Kab. Raja ampat",
      latitude: -1.0915,
      longitude: 130.8779
    },
    {
      city: "Kab. Sorong",
      latitude: -1.1223,
      longitude: 131.4883
    },
    {
      city: "Kab. Sorong selatan",
      latitude: -1.7658,
      longitude: 132.1573
    },
    {
      city: "Kab. Tambrauw",
      latitude: -0.7819,
      longitude: 132.3938
    },
    {
      city: "Kab. Teluk bintuni",
      latitude: -1.9057,
      longitude: 133.3295
    },
    {
      city: "Kab. Teluk wondama",
      latitude: -2.8552,
      longitude: 134.3237
    },
    {
      city: "Kota sorong",
      latitude: -0.8762,
      longitude: 131.2558
    }
   ]