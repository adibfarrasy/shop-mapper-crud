module.exports.descriptors = [
    'Vintage',
    'Modern',
    'Silent',
    'Bustling',
    'Sea',
    'Forest',
    'Mountain',
    'Beach',
    'Downtown',
    'Alley',
]

module.exports.places = [
    'Coffee Shop',
    'Village',
    'Hills',
    'Flower Garden',
    'Backcountry',
    'River',
    'Creek',
    'Spot',
    'Antique Shop',
    'Cafeteria',
    'Eats',
    'Old Town'
]