const Shop = require("../models/shop");
const mbxGeocoding = require("@mapbox/mapbox-sdk/services/geocoding");
const mapBoxToken = process.env.MAPBOX_TOKEN;
const geocoder = mbxGeocoding({ accessToken: mapBoxToken });
const { cloudinary } = require("../cloudinary");

module.exports.index = async (req, res) => {
  const shops = await Shop.find({});
  res.render("shops/index", { shops });
};

module.exports.renderNewForm = (req, res) => {
  res.render("shops/new");
};

module.exports.createShop = async (req, res, next) => {
  const geoData = await geocoder
    .forwardGeocode({
      query: req.body.shop.location,
      limit: 1,
    })
    .send();

  const shop_obj = new Shop(req.body.shop);
  shop_obj.geometry = geoData.body.features[0].geometry;
  shop_obj.images = req.files.map((f) => ({
    url: f.path,
    filename: f.filename,
  }));
  shop_obj.author = req.user._id;
  await shop_obj.save();
  req.flash("success", "Successfully created a new shop.");
  res.redirect(`/shops/${shop_obj._id}`);
};

module.exports.showShop = async (req, res) => {
  const shop = await Shop.findById(req.params.id)
    .populate({
      path: "reviews",
      populate: {
        path: "author",
      },
    })
    .populate("author");
  if (!shop) {
    req.flash("error", "Cannot find the shop.");
    return res.redirect("/shops");
  }
  res.render("shops/show", { shop });
};

module.exports.renderEditForm = async (req, res) => {
  const shop = await Shop.findById(req.params.id);

  if (!shop) {
    req.flash("error", "Cannot find the shop.");
    return res.redirect("/shops");
  }

  res.render("shops/edit", { shop });
};

module.exports.updateShop = async (req, res) => {
  const { id } = req.params;

  const shop = await Shop.findByIdAndUpdate(id, { ...req.body.shop });
  const imgs = req.files.map((f) => ({
    url: f.path,
    filename: f.filename,
  }));
  shop.images.push(...imgs);

  if (req.body.deleteImages) {
    for (let filename of req.body.deleteImages) {
      await cloudinary.uploader.destroy(filename);
    }
    await shop.updateOne({
      $pull: { images: { filename: { $in: req.body.deleteImages } } },
    });
  }

  await shop.save();
  req.flash("success", "Successfully updated the shop.");
  res.redirect(`/shops/${shop._id}`);
};

module.exports.deleteShop = async (req, res) => {
  const { id } = req.params;
  await Shop.findByIdAndDelete(id);
  req.flash("success", "Successfully deleted the shop.");
  res.redirect("/shops");
};
